# django-server

Web server in `django` written using Python 3.

- Configured for Heroku deployment with `gunicorn`
- Contains some static files, templates, views and models interacting with each other written mostly to get a hang of web development
